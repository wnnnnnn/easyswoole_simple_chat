const request = require('../../utils/request.js');
var app = getApp();
Page({
  data: {
    list: []
  },
  onLoad: function() {
    this.user_list()
  },
  user_list: function() {
    var _this = this
    request.requests('getUserList', {}, 'get', function(data) {
      _this.setData({
        list: data.result
      })
    });
  },
  gotoinfo: function(ress) {
    wx.getSetting({
      success(res) {
        if (!res.authSetting['scope.userInfo']) {
          wx.navigateTo({
            url: '/pages/auth/auth',
          })
        } else {
          app.upload_info(app)
          wx.navigateTo({
            url: '/pages/chat/chat?id=' + ress.currentTarget.dataset.id,
          })
        }
      }
    });


  }
})
// pages/problem/problem.js
const request = require('../../utils/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    question:[],
    lent:1,
    dangqian:0,
    nowquest:[]
  },
  next:function(option){
    var that = this
    var dangqian = ++that.data.dangqian
    this.setData({
      dangqian: dangqian,
      nowquest: that.data.question[dangqian]
    })

  },
  sub:function(){
    arr = {}
  
    arr['1'] = 7;
    arr['2'] = 9;
    arr['3'] = 6;
    arr['4'] = 9;
    arr['5'] = 9;
    arr['6'] = 9;
    arr['7'] = 7;
    arr['8'] =4;
    arr['9'] = 9;
    arrs=JSON.stringify(arr).toString();
    console.log(arrs)
    request.requests('submit_quest', {
      answer:arrs
    }, 'post', function (data) {

      
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    request.requests('question', {
      user_id: 1
    }, 'get', function (data) {

      that.setData({
        question:data.result,
        lent:data.result.length,
        nowquest:data.result[0]
      })

      console.log(data.result)
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
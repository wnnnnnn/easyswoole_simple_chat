const app = new getApp();
const request = require('../../utils/request.js');

Page({
  dati:function(){
    wx.navigateTo({
      url: '/pages/problem/problem'
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    chat_list: [],
    my_info:[],
    islogin:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    
    var _this = this;
    getApp().login().then(res => {
      _this.setData({
        islogin : 1,
        my_info: app.globalData.userInfo
      })
      _this.getlist(_this)
      _this.up()
    });
    
  },
  up(){
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          app.upload_info(app)
        }
      }
    });
  },
  onShow:function(){
    var that = this
    if(this.data.islogin == 1){
      this.getlist(this)
    }
    wx.onSocketMessage(function (data) {
      console.log('监听、·')
      var msg = JSON.parse(data.data)
      switch (msg['type']) {
        case 'msg':
          that.getlist(that)
          break;
      }
    })
  },
  getlist: function (_this) {
    //获取最近的消息列表
    console.log('sssss',app.globalData.userInfo.id)
    request.requests('chatList', {
      user_id: app.globalData.userInfo.id
    }, 'get', function (data) {
      _this.setData({
        chat_list: data.result
      })
    });
  },

  gotoinfo: function(res) {
    console.log(res)
    wx.navigateTo({
      url: '/pages/chat/chat?id=' + res.currentTarget.dataset.id,
    })
  }


})
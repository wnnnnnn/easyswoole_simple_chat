const app = new getApp();
const request = require('../../utils/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chat_list:[],
    my_info:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      my_info: app.globalData.userInfo
    })
    this.getlist()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var _this = this
    this.getlist()
    wx.onSocketMessage(function (data) {
      console.log('lll',data.data)
      var msg = JSON.parse(data.data)
      switch (msg['type']) {
        case 'msg':
          _this.getlist()
          break;
      }
    })
    
  },
  
  gotoinfo: function (res) {
    console.log(res)
    wx.navigateTo({
      url: '/pages/chat/chat?id=' + res.currentTarget.dataset.id,
    })
  },


})
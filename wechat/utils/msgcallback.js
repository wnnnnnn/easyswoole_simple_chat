function msgcallback(from_id,send_id) {
  console.log(send_id)
  wx.sendSocketMessage({
    data: JSON.stringify({
      type: 'msgcallback',
      from_id,
      send_id
    })
  })
}

module.exports.msgcallback = msgcallback;
var app = getApp();
function requests(url, data, methodType, cb) {
  data['user_id'] = getApp().globalData.userInfo.id
  wx.showNavigationBarLoading();
  wx.request({
    method: methodType,
    url: 'https://www.wnlzy.top/'+url,
    data: data,
    success: function (res) {
      typeof cb == "function" && cb(res.data, "");
      wx.hideNavigationBarLoading();
    },
    fail: function (err) {
      wx.showModal({
        title: '提示',
        content: '呀，一不小心出错啦~',
        showCancel: false
      });
      wx.hideNavigationBarLoading();
    }
  });
}

module.exports.requests = requests;
//app.js
App({
  onLaunch() {
    const updateManager = wx.getUpdateManager()
    updateManager.onCheckForUpdate(function(res) {
      // 请求完新版本信息的回调
      if (res.hasUpdate) {
        wx.showToast({
          title: '检测到新版本,稍后将进行重启',
          icon: 'none'
        })
        updateManager.onUpdateReady(function() {
          updateManager.applyUpdate()
        })
        updateManager.onUpdateFailed(function() {
          updateManager.applyUpdate()
        })
      }
    })

  },
  login() {
    var _this = this
    return new Promise((resolve, reject) => {
      wx.login({
        success: res => {
          var info = {
            code: res.code
          }
          wx.request({
            method: 'get',
            url: 'https://www.wnlzy.top/api/user/login',
            data: info,
            success: function(res) {
              console.log('登录', res)
              if (res.data.code) {
                wx.showToast({
                  title: '登入成功',
                  icon: 'success',
                  duration: 2000
                })
                _this.globalData.userInfo = {
                  id: res.data.result
                }
                wx.connectSocket({
                  url: 'wss://www.wnlzy.top?id=' + res.data.result,
                })
                // wx.onSocketClose(function(){
                //   console.log('监听到链接关闭啦')
                //   wx.connectSocket({
                //     url: 'wss://www.wnlzy.top:2345/?id=' + res.data.content,
                //   })
                // })
                resolve();
              }
              wx.hideNavigationBarLoading();
            },
            fail: function(err) {
              wx.showModal({
                title: '提示',
                content: '呀，一不小心出错啦~',
                showCancel: false
              });
              wx.hideNavigationBarLoading();
            }
          });
        }
      })
    })
  },
  upload_info(app) {
    wx.getUserInfo({
      success(getinfo) {
        var info = {
          img: getinfo.userInfo.avatarUrl,
          name: getinfo.userInfo.nickName,
          id: app.globalData.userInfo.id,
        }
       app.globalData.userInfo = info
        wx.request({
          method: 'post',
          url: 'https://www.wnlzy.top/updateInfo',
          data: info,
          success: function(res) {
            wx.hideNavigationBarLoading();
          },
          fail: function(err) {
            wx.showModal({
              title: '提示',
              content: '呀，一不小心出错啦~',
              showCancel: false
            });
            wx.hideNavigationBarLoading();
          }
        });
      },
      fail() {
        console.log('获取信息失败啦')
      }
    })
  },
  globalData: {
    userInfo: [],
    ws: null
  },

})
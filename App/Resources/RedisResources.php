<?php


namespace App\Resources;


/**
 * 此处应该用单例，或者连接池啥的，懒得写了
 * Class RedisResources
 * @package App\HttpController
 */
class RedisResources
{
    protected $redis;
    public function __construct()
    {
        if (!$this->redis){
            $this->redis = new \EasySwoole\Redis\Redis(new \EasySwoole\Redis\Config\RedisConfig([
                'host' => 'www.wnlzy.top',
                'port' => '6379',
                'serialize' => \EasySwoole\Redis\Config\RedisConfig::SERIALIZE_NONE
            ]));
        }
    }
    public function link(){
        return $this->redis;
    }

}

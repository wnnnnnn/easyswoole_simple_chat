<?php
/**
 * Created by PhpStorm.
 * User: Mr wang
 * Date: 2019/11/11
 * Time: 下午2:08
 */
namespace App\HttpController;

use App\HttpController\Router\Message;
use App\HttpController\Router\Test;
use App\HttpController\Router\User;
use EasySwoole\Http\AbstractInterface\AbstractRouter;
use FastRoute\RouteCollector;

class Router extends AbstractRouter{

    function initialize(RouteCollector $routeCollector)
    {
        User::add($routeCollector);
        Message::add($routeCollector);
    }
}

<?php

namespace App\HttpController\Api;

use App\HttpController\BaseController;
use App\Model\MessageModel;
use App\Resources\RedisResources;

class Message extends BaseController
{
    /**
     * @description
     * @author Mr wang  2019/11/16 下午4:08
     */
    public function chatList()
    {
        $redisLink = (new RedisResources())->link();
        $userId = $this->request()->getRequestParam('user_id');
        $chatList = $redisLink->get('clist_' . $userId) ?? '[]';
        $this->writeJson(200, '消息列表', json_decode($chatList));
    }

    /**
     * 聊天记录列表
     * @description
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     * @author Mr wang  2019/11/16 下午3:41
     */
    public function messageList()
    {
        $params = $this->request()->getRequestParam();
        $where = "(send_id={$params['send_id']} and from_id = {$params['from_id']}) or (send_id={$params['from_id']} and from_id = {$params['send_id']})";
        $messageList = MessageModel::create()->where($where)->select();
        $this->writeJson(200, '消息详情列表', $messageList);
    }

    /**
     * 初始化消息类型
     * @description
     * @author Mr wang  2019/11/17 0:15
     * @param $server
     * @param $fd
     * @param $data
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \EasySwoole\Redis\Exception\RedisException
     * @throws \Throwable
     */
    public function initMessage($server, $fd, $data)
    {
        @$data = json_decode($data, true);


        $data['send_time'] = date('Y-m-d H:i:s');
        switch ($data['type']) {
            case MSG :
                $this->sendClient($server, $data);
                break;
            case READ_BACK :
                $this->msgRead($data);
                break;
        }
    }

    /**
     * 消息发送
     * @description
     * @author Mr wang  2019/11/17 0:15
     * @param $server
     * @param $data
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \EasySwoole\Redis\Exception\RedisException
     * @throws \Throwable
     */
    public function sendClient($server, $data)
    {
        print_r('走到转发了' . PHP_EOL);
        //同步mysql储存消息
        $this->saveMessage($data);
        //生成会话列表 同时给 发送者 和 接收者生成/更新 会话
        $this->chatListUpdate($data);

        $send_id = $data['send_id'];
        //查询接收者是否在线
        $redisLink = (new RedisResources())->link();
        $send_fd = $redisLink->get('user_' . $send_id);
        if ($send_fd) {
            print_r('我要转发消息给' . $send_id . PHP_EOL);
            $server->push($send_fd, json_encode($data));
        }
    }

    /**
     * 保存消息
     * @description
     * @param $data
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     * @author Mr wang  2019/11/16 下午3:54
     */
    public function saveMessage($data)
    {
        MessageModel::create([
            'send_id' => $data['send_id'],
            'from_id' => $data['from_id'],
            'content' => $data['content']
        ])->save();
    }

    /**
     * 更新消息列表
     * @description
     * @param $data
     * @throws \EasySwoole\Redis\Exception\RedisException
     * @author Mr wang  2019/11/16 下午4:01
     */
    public function chatListUpdate($data)
    {
        $redisLink = (new RedisResources())->link();

        $from_id = $data['from_id'];
        $send_id = $data['send_id'];

        //更新接收者未读会话
        //获取接收者的会话列表
        $send_list = $redisLink->get('clist_' . $send_id) ?? '[]';

        $send_list = json_decode($send_list,true);

        unset($send_list['id_'.$from_id]);
        $send_arr['id_'.$from_id] = $data;
        $send_arr['id_'.$from_id]['is_read'] = 0;
        $send_arr['id_'.$from_id]['time'] = date('Y-m-d');
        // $send_list[$from_id] = $data;
        // $send_list[$from_id]['is_read'] = 0; //接收者的对应该发送者发送的信息设置为未读
        $send_list = array_merge($send_arr,$send_list);

        $redisLink->set('clist_' . $send_id, json_encode($send_list)); //clist_1_93

        //给发送者增加会话列表
        $from_list = $redisLink->get('clist_' . $from_id) ?? "[]"; //clist_1_1395获取发送者的会话列表
        $from_list = json_decode($from_list,true);


        unset($from_list['id_'.$send_id]);
        $from_arr['id_'.$send_id] = $data;
        $from_arr['id_'.$send_id]['is_read'] = 1;
        $from_arr['id_'.$send_id]['time'] = date('Y-m-d');;
        // $from_list[$send_id] = $data;
        // $from_list[$send_id]['is_read'] = 1;
        $from_list = array_merge($from_arr,$from_list);
        $redisLink->set('clist_' . $from_id, json_encode($from_list));
    }

    /**
     * 将会话列表设置为已读
     * @description
     * @author Mr wang  2019/11/17 0:15
     * @param $data
     * @throws \EasySwoole\Redis\Exception\RedisException
     */
    public static function msgRead($data)
    {
        $redisLink = (new RedisResources())->link();

        $from_id = $data['from_id'];
        $send_id = $data['send_id'];

        $send_list = $redisLink->get('clist_' . $send_id)??'[]';
        $send_list = json_decode($send_list,true);
        if ($send_list && isset($send_list['id_'.$from_id])) {
            $send_list['id_'.$from_id]['is_read'] = 1;
            $redisLink->set('clist_'. $send_id, json_encode($send_list));
        }
    }


}

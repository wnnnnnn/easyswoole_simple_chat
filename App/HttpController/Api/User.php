<?php

namespace App\HttpController\Api;

use App\HttpController\BaseController;
use App\Resources\RedisResources;
use App\Model\UserModel;
use App\Validate\UserValidate;

class User extends BaseController
{

    /**
     * 用户登录
     * @description
     * @author Mr wang  2019/11/16 下午1:16
     * @return bool
     * @throws \EasySwoole\Mysqli\Exception\Exception
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function login(){
        $validate = (new UserValidate())->secnceName('login')->check($this->request()->getRequestParam());
        if ($validate !== true){
            $this->writeJson(400,$validate);
            return false;
        }
        $wechat = wechatLogin($this->request()->getRequestParam('code'));
        if($wechat == NULL || isset($wechat['errcode'])){
            $this->writeJson(400,'登录失败');
        }else{
            //查询表中是否有此值
            $user_info = UserModel::create()->where('open_id',$wechat['openid'])->findOne();
            if(empty($user_info)){
                $data['open_id'] = $wechat['openid'];
                $id = UserModel::create($data)->save();
            }else{
                $id = $user_info['id'];
            }
            if ($id === null){
                $this->writeJson(400,'登录失败,服务器插入错误');
            }else{
                $this->writeJson(200,'登录成功',$id);
            }
        }
    }

    public function getUserList(){
        $userList = UserModel::create()->where('name','','!=')->select();
        $this->writeJson(200,'用户列表',$userList);
    }

    public function userInfo(){
        $userInfo = UserModel::create()->where('id',$this->request()->getRequestParam('id'))->findOne();
        $this->writeJson(200,'用户详情',$userInfo);
    }

    public function updateInfo(){
        $params = json_decode($this->request()->getBody()->__toString(),true);
        $validate = (new UserValidate())->secnceName('update')->check($params);
        if ($validate !== true){
            $this->writeJson(400,$validate);
            return false;
        }
        UserModel::create()->update($params, ['id' => $params['id']]);
    }

    /**
     * 使用redis进行用户和fd的绑定
     * @description
     * @author Mr wang  2019/11/16 下午1:35
     * @param $fd
     * @param $userId
     * @throws \EasySwoole\Redis\Exception\RedisException
     */
    public function userBindFd($userId,$fd){
        $redisLink = (new RedisResources())->link();
        $redisLink->set('user_'.$userId,$fd,10800);
        print_r("绑定user".$fd);
        print_r("绑定fd".$userId);
        $redisLink->set('fd_'.$fd,$userId,10800);
    }

    /**
     * 解除绑定关系
     * @description
     * @author Mr wang  2019/11/16 下午1:43
     * @param $fd
     */
    public function userUnBindFd($fd){
        $redisLink = (new RedisResources())->link();
        //首先根据fd获取用户id
        $userId = $redisLink->get('fd_'.$fd);
        // 如果获取到，解除绑定关系
        if (!empty($userId)){
            $redisLink->del('fd'.$fd);
            $redisLink->del('user'.$userId);
        }
    }
}

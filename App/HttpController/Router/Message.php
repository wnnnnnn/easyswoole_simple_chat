<?php
/**
 * Created by PhpStorm.
 * User: Mr wang
 * Date: 2019/11/11
 * Time: 下午2:08
 */
namespace App\HttpController\Router;

use FastRoute\RouteCollector;

class Message{
    public static function add(RouteCollector $routeCollector){
        $routeCollector->get('/messageList', '/Api/Message/messageList');
        $routeCollector->get('/chatList', '/Api/Message/chatList');
    }
}

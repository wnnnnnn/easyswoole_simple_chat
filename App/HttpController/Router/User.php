<?php
/**
 * Created by PhpStorm.
 * User: Mr wang
 * Date: 2019/11/11
 * Time: 下午2:08
 */
namespace App\HttpController\Router;

use FastRoute\RouteCollector;

class User{
    public static function add(RouteCollector $routeCollector){
        $routeCollector->get('/getUserList', '/Api/User/getUserList');
        $routeCollector->post('/updateInfo', '/Api/User/updateInfo');
        $routeCollector->get('/userInfo', '/Api/User/userInfo');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Mr wang
 * Date: 2019/11/14
 * Time: 下午5:28
 */

namespace App\Validate;

use EasySwoole\Validate\Validate;

class UserValidate extends BaseValidate
{
    protected $rule = [
        'code' => [
            'required' => ['code不能为空'],
        ],
        'img' => [
            'required' => ['头像不能为空'],
        ],
        'name' => [
            'required' => ['名字不能为空'],
        ]
    ];
    protected $secnce = [
        'login' => ['code'],
        'update' => ['img','name']
    ];


}

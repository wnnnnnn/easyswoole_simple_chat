<?php
/**
 * Created by PhpStorm.
 * User: Mr wang
 * Date: 2019/11/14
 * Time: 下午5:28
 */

namespace App\Validate;

use EasySwoole\Http\AbstractInterface\Controller;
use EasySwoole\Validate\Validate;

/**
 * 封装验证器
 * Class BaseValidate
 * @package App\Validate
 */
class BaseValidate extends Controller
{
    protected $validate;
    protected $secnceName;

    public function index(){}

    public function secnceName($secnceName = '')
    {
        $this->validate = new Validate();
        $this->secnceName = $secnceName;
        return $this;
    }

    public function check($data)
    {
        $this->addColumn();
        $bool = $this->validate->validate($data);
        return $bool ? $bool : $this->validate->getError()->__toString();
    }

    public function addColumn()
    {
        //获取规则
        $rule = $this->rule;
        //获取场景
        $secnec = $this->secnce[$this->secnceName];

        foreach ($secnec as $value) {
            $addColumn = $rule[$value];
            $thisValidate = $this->validate->addColumn($value, $value);
            foreach ($addColumn as $key => $type) {
                $this->addType($thisValidate, $key, $type);
            }
        }
    }

    public function addType($thisValidate, $key, $type)
    {
        $thisValidate->$key(...$type);
    }
}

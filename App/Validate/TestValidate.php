<?php
/**
 * Created by PhpStorm.
 * User: Mr wang
 * Date: 2019/11/14
 * Time: 下午5:28
 */

namespace App\Validate;


use EasySwoole\Validate\Validate;

class TestValidate extends BaseValidate
{
    protected $rule = [
        'name' =>[
            'required' => ['姓名不能为空'],
        ],
        'age' => [
            'required' => ['年龄不能为空']
        ]
    ];
    protected $secnce = [
        'add' => ['name','age']
    ];


}

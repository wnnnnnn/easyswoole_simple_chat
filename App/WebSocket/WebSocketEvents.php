<?php

namespace App\WebSocket;


use App\HttpController\Api\Message;
use App\HttpController\Api\User;

/**
 * WebSocket Events
 * Class WebSocketEvents
 * @package App\WebSocket
 */
class WebSocketEvents
{
    /**
     * 打开了一个链接
     * @param swoole_websocket_server $server
     * @param swoole_http_request $request
     */
    static function onOpen($server, $request)
    {
        print_r('id为:'.$request->get['id'].'的用户连接上了,fd:'.$request->fd.PHP_EOL);
        (new User())->userBindFd($request->get['id'],$request->fd);
    }

    static function onMessage($server, $frame){
        try{
            (new Message())->initMessage($server,$frame->fd,$frame->data);
        }catch (Exception $e){
            print_r($e->getMessage());
        }


    }

    /**
     * 链接被关闭时
     * @param swoole_server $server
     * @param int $fd
     * @throws Exception
     */
    static function onClose($server, int $fd)
    {
        /** @var array $info */
        $info = $server->getClientInfo($fd);
        /**
         * 判断此fd 是否是一个有效的 websocket 连接
         * 参见 https://wiki.swoole.com/wiki/page/490.html
         */
        if ($info && $info['websocket_status'] === WEBSOCKET_STATUS_FRAME) {
            /**
             * 判断连接是否是 server 主动关闭
             * 参见 https://wiki.swoole.com/wiki/page/p-event/onClose.html
             */
            print_r('fd为:'.$fd.'的用户断开了连接'.PHP_EOL);
            (new User())->userUnBindFd($fd);
        }

    }


}

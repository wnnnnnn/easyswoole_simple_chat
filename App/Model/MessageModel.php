<?php
/**
 * Created by PhpStorm.
 * User: Mr wang
 * Date: 2019/11/12
 * Time: 下午4:14
 */
namespace App\Model;

use EasySwoole\ORM\AbstractModel;

class MessageModel extends AbstractModel
{
    /**
     * 数据表名 当没有做定义的时候,自动将类名转为下划线格式作为表名
     * @var string
     */
    protected $tableName = 'message';
}


/*
Navicat MySQL Data Transfer

Source Server         : 王宁im
Source Server Version : 50725
Source Host           : www.wnlzy.top:3306
Source Database       : im

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-11-17 00:18:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `send_id` int(11) NOT NULL COMMENT '发送者id',
  `from_id` int(11) NOT NULL COMMENT '接收者id',
  `content` varchar(255) DEFAULT NULL,
  `is_read` tinyint(5) NOT NULL DEFAULT '0' COMMENT '是否已读 1=已读 0=未读',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `open_id` varchar(255) DEFAULT '' COMMENT '小程序openid',
  `name` varchar(255) DEFAULT '' COMMENT '名称',
  `img` varchar(255) DEFAULT '' COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
